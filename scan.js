require('dotenv').config({
    path: __dirname + '/.env'
});

const { Blockchain } = require('btc-core');
const { Database } = require('db-core');

const blockchain = new Blockchain(
    process.env.RPC_USERNAME,
    process.env.RPC_PASSWORD,
    process.env.RPC_HOST,
    process.env.RPC_PORT
);

const database = new Database(
    `${process.env.DB_CONNECTOR}:dbname=${process.env.DB_NAME};host=${process.env.DB_HOST};port=${process.env.DB_PORT};`,
    process.env.DB_USERNAME,
    process.env.DB_PASSWORD
);

let now;

const COINBASE = 'coinbase';
let ADDRESSES = {
    length: 0
};
let HASHES = {
    length: 0
};
let MAX_SCAN = process.env.BLOCK_MAX_SCAN;
const TX_MAX_SCAN = process.env.TX_MAX_SCAN;
const BLOCK_LOG_EVERY = process.env.BLOCK_LOG_EVERY;
let finished = false;
let transactions = [];
let blockNtx = 0;
let blockchainBatchTime = 0;

async function getTransaction (entry) {
    let {address, value, time, block, hash} = entry;
    address =  await getAddressHash(address);
    hash = await getHash(hash);
    return {
        hash,
        address,
        value,
        time,
        block
    };
}

async function getAddressHash(hash) {
    let _id = ADDRESSES[hash];
    if (_id) {
        if (ADDRESSES.length >= 5000) {
            ADDRESSES = {
                length: 0
            };
        }
        return _id;
    }
    let [ {id } ] = await database.query(
        `SELECT get_address_id_by_name('${hash}') as id`
    );
    ADDRESSES.length ++;
    return ADDRESSES[hash] = id;
}

async function getHash(hash) {
    let _id = HASHES[hash];
    if (_id) {
        if (HASHES.length > 1000) { // flush
            HASHES = {
                length: 0
            };
        }
        return _id;
    }
    let [ {id } ] = await database.query(
        `SELECT get_hash_id_by_hash('${hash}') as id`
    );
    HASHES.length ++;
    return HASHES[hash] = id;
}

function logtime() {
    console.log('TIME: ' + ((Date.now() - now) / 1000));
}

function logblock(no) {
    console.log('LAST BLOCK: ' + no);
}

async function insert () {
    if (finished && !transactions.length) {
        logtime();
        console.log(((new Date).toLocaleString()) + ' END');
        process.exit();
    }
    if (transactions.length) {
        try {
            let sql = 'INSERT INTO transactions(hash_id,address_id,qty,created_at,block_no,price,amount)VALUES';
            let first = true;
            for (let entry of transactions.splice(0, 1000)) {
                let {address, value, time, block, hash} = await getTransaction(entry);
                if (!first) {
                    sql += ',';
                }
                sql += `(${hash},${address},${value},FROM_UNIXTIME(${time}),${block},0,0)`;
                first = false;
            }
            await database.query(sql);
        } catch (e) {
            logtime();
            console.log(e);
            process.exit();
        }
    }
    setTimeout(insert, 0);
}

function round (value) {
    return Math.round(value * 100) / 100;
}

async function lastBlock (start, end) {
    let [ {id } ] = await database.query(
        `SELECT get_last_block(${start},${end}) as id`
    );
    return id;
}

function getAddress(entry) {
    const { scriptPubKey } = entry;
    let addresses = scriptPubKey.addresses || [scriptPubKey.type];
    if (scriptPubKey.type == 'multisig' && addresses.length > 1) { // revisar https://live.blockcypher.com/btc/tx/60a20bd93aa49ab4b28d514ec10b06e1829ce6818ec06cd3aabd013ebcdc4bb1/ es un ejemplo el resulve la de destino
        addresses = [scriptPubKey.type];
    }
    if (!addresses.length || addresses.length > 1) {
        console.log(entry);
        throw new Error('REVISAR DIRECCIONES');
    }
    return addresses[0];
}

async function getBlockTransactions (noOrHash, out) {
    const block = await blockchain.block(noOrHash);
    let { tx, height, nextblockhash, time } = block;
    out.nextblockhash = nextblockhash;
    out.height = height;
    let nTx = 0;
    let rows;
    while (rows = tx.splice(0, TX_MAX_SCAN)) {
        if (!rows.length) {
            break;
        }
        let params = [];
        for (let hash of rows) {
            params.push([hash, true]);
            nTx ++;
        }
        const vouts = {};
        const inputs = [];
        for await (let txOut of blockchain.batch('getrawtransaction', params)) {
            const { vin, vout} = txOut;
            let hash = txOut.txid;
            for (let entry of vout) {
                transactions.push({
                    address: getAddress(entry),
                    value: entry.value,
                    time,
                    block: height,
                    hash
                });
            }
            for (let entry of vin) {
                if (entry.hasOwnProperty(COINBASE)) {
                    if (vin.length > 1) {
                        throw new Error('Invalid coinbase input');
                    }
                    transactions.push({
                        address: COINBASE,
                        value: - vout.reduce((acc, entry) => acc + entry.value, 0),
                        time,
                        block: height,
                        hash
                    });
                } else if (entry.hasOwnProperty('txid')) {
                    let { txid, vout } = entry;
                    if (!vouts.hasOwnProperty(txid)) {
                        inputs.push(txid);
                        vouts[txid] = [];
                    }
                    vouts[txid].push({txid: hash, index: vout});
                } else {
                    throw new Error('Invalid from');
                }
            }
        }
        if (inputs.length) {
            let _rows;
            while (_rows = inputs.splice(0, TX_MAX_SCAN)) {
                if (!_rows.length) {
                    break;
                }
                let params = [];
                for (let hash of _rows) {
                    params.push([hash, true]);
                    nTx ++;
                }
                for await (let entry of blockchain.batch('getrawtransaction', params)) {
                    const { vout } = entry;
                    const hash = entry.txid;
                    for (const { txid, index } of vouts[hash]) {
                        const output = vout[index];
                        transactions.push({
                            address: getAddress(output),
                            value: - output.value,
                            time,
                            block: height,
                            hash: txid
                        });
                    }
                }
            }
        }
    }
    out.nTx = nTx;
}

async function loop (numOrHash, counter, blockNum) {
    if (counter > MAX_SCAN || blockNum > process.env.BLOCK_END_SCAN) {
        logtime();
        logblock(blockNum - 1);
        finished = true;
        return;
    }
    let block = { nextblockhash: null, height: null };
    try {
        let initBatchTime = Date.now();
        await getBlockTransactions(numOrHash, block);
        blockchainBatchTime += Date.now() - initBatchTime;
        blockNtx += block.nTx;
        if (counter % BLOCK_LOG_EVERY === 0) {
            let curNow = Date.now();
            let timeTotal = round((curNow - now) / 1000);
            console.log('| ' + ((new Date).toLocaleString()) + ' | LAST BLOCK: ' +
                block.height + ' | ' + counter + ' BLOCKS | ' +
                timeTotal + ' SEC | ' +
                round(timeTotal / BLOCK_LOG_EVERY) + ' Sec/Blk' + ' | ' +
                'Node ' + round((blockchainBatchTime / 1000) / BLOCK_LOG_EVERY) + ' Sec/Blk | '  +
                blockNtx + '  TRANS | ' +
                round(blockNtx / timeTotal) + ' Tr/s | '
            );
            blockNtx = 0;
            blockchainBatchTime = 0;
            now = Date.now();
        }
    } catch (e) {
        logtime();
        logblock(blockNum);
        console.log(e);
        process.exit();
    }
    if (block && block.nextblockhash) {
        lastNow = Date.now();
        setTimeout(loop.bind(null, block.nextblockhash,  ++ counter, block.height + 1), 0);
    } else {
        logtime();
        logblock(blockNum);
        process.exit();
    }
}

setTimeout(async () => {
    if (process.argv.length === 3) {
        let max = parseInt(process.argv[2]);
        if (max > 0) {
            MAX_SCAN = max;
        }
    }
    const start = await lastBlock(process.env.BLOCK_START_SCAN, process.env.BLOCK_END_SCAN);
    console.log(((new Date).toLocaleString()) + ' START BLOCK ' + start);
    lastNow = now = Date.now();
    setTimeout(loop.bind(null, start, 1, start), 0);
    setTimeout(insert, 0);
}, 0);

/*
setTimeout(async () => {
    let block = { nextblockhash: null };
    const now = Date.now();
    await getBlockTransactions(366061, block);
    for (let entry of transactions) {
        console.log(entry);
        break;
    }
    console.log('TIME: ' + ((Date.now() - now) / 1000));
    console.log(block);
}, 0);
*/
/*
setTimeout(async () => {
    const start = await lastBlock(process.env.BLOCK_START_SCAN, process.env.BLOCK_END_SCAN);
    console.log('START: ' + start);
});
*/
