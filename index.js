const { Blockchain, Block } = require('btc-core');
const { Database } = require('db-core');
require('dotenv').config();
let now;
let lastNow;
const blockchain = new Blockchain(process.env.RPC_USERNAME, process.env.RPC_PASSWORD);
const database = new Database(
    `mysql:dbname=${process.env.DB_NAME}`,
    process.env.DB_USERNAME,
    process.env.DB_PASSWORD
);

const ADDRESSES = {};
const HASHES = {};

let MAX_SCAN = 10000;
let finished = false;
let transactions = [];

async function importTransaction (entry) {
    let {address, value, time, block, hash} = entry;
    let [ {id } ] = await database.query(
        `SELECT import_transaction('${hash}', '${address}', ${value}, ${time}, ${block}) as id`
    );
    return id;
}

async function getTransaction (entry) {
    let {address, value, time, block, hash} = entry;
    address =  await getAddressHash(address);
    hash = await getHash(hash);
    return {
        hash,
        address,
        value,
        time,
        block
    };
}

async function getAddressHash(hash) {
    if (ADDRESSES.hasOwnProperty(hash)) {
        return ADDRESSES[hash];
    }
    let [ {id } ] = await database.query(
        `SELECT get_address_id_by_name('${hash}') as id`
    );
    return ADDRESSES[hash] = id;
}

async function getHash(hash) {
    if (HASHES.hasOwnProperty(hash)) {
        return HASHES[hash];
    }
    let [ {id } ] = await database.query(
        `SELECT get_hash_id_by_hash('${hash}') as id`
    );
    return HASHES[hash] = id;
}

function logtime() {
    console.log('TIME: ' + ((Date.now() - now) / 1000));
}

function logblock(no) {
    console.log('LAST BLOCK: ' + no);
}

async function insert () {
    if (finished && !transactions.length) {
        logtime();
        console.log(((new Date).toLocaleString()) + ' END');
        process.exit();
    }
    if (transactions.length) {
        const entry = transactions.shift();
        try {
            await importTransaction(entry);
        } catch (e) {
            logtime();
            console.log(e);
            process.exit();
        }
    }
    setTimeout(insert, 0);
}

async function loop (numOrHash, counter, blockNum) {
    if (counter > MAX_SCAN) {
        logtime();
        logblock(blockNum - 1);
        finished = true;
        return;
    }
    let block;
    try {
        block = await new Block(blockchain).get(numOrHash);
        // console.log(((new Date).toLocaleString()) + ' BLOCK: ' + block.height);
        for await (let transaction of block.tx) {
            transactions.push(await getTransaction(transaction));
        }
        if (counter % 100 === 0) {
            let curNow = Date.now();
            let timeNow = ((curNow - lastNow) / 1000);
            let timeTotal = (curNow - now) / 1000;
            console.log('| ' + ((new Date).toLocaleString()) + ' | LAST BLOCK: ' +
                block.height + ' | 100 BLOCKS IN ' + round(timeNow) + ' SEC ->' +
                ' 1 BLOCK IN ' + round(timeNow / 100) + ' SEC |' +
                ' ' + counter + ' BLOCKS IN' +
                ' ' + round(timeTotal) + ' SEC ->' +
                ' 1 BLOCK IN ' + round(timeTotal / counter) + ' SEC |'
            );
        }
    } catch (e) {
        logtime();
        logblock(blockNum);
        console.log(e);
        process.exit();
    }
    if (block && block.nextblockhash) {
        lastNow = Date.now();
        setTimeout(loop.bind(null, block.nextblockhash,  ++ counter, block.height + 1), 0);
    } else {
        logtime();
        logblock(blockNum);
        process.exit();
    }
}

function round (value) {
    return Math.round(value * 100) / 100;
}

async function lastBlock () {
    let [ {id } ] = await database.query(
        `SELECT get_last_block() as id`
    );
    return id;
}

if (process.argv.length === 3) {
    let max = parseInt(process.argv[3]);
    if (max > 0) {
        MAX_SCAN = max;
    }
}

setTimeout(async () => {
    const start = await lastBlock();
    console.log(((new Date).toLocaleString()) + ' START BLOCK ' + start);
    lastNow = now = Date.now();
    setTimeout(loop.bind(null, start, 1, start), 0);
    setTimeout(insert, 0);
}, 0);

