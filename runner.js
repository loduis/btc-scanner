require('dotenv').config();
const Scanner = require('./scanner');

setTimeout(async () => {
    let block = { nextblockhash: null };
    const now = Date.now();
    for await (let entry of Scanner.getBlockTransactions(237945, block)) {
        console.log(entry);
    }
    console.log('TIME: ' + ((Date.now() - now) / 1000));
    console.log(block);
}, 0)
