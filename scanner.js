const { Blockchain } = require('btc-core');
const COINBASE = 'coinbase';
const blockchain = new Blockchain(process.env.RPC_USERNAME, process.env.RPC_PASSWORD);

function getAddresses(entry) {
    const { scriptPubKey } = entry;
    let addresses = scriptPubKey.addresses || [scriptPubKey.type];
    if (scriptPubKey.type == 'multisig' && addresses.length > 1) { // revisar https://live.blockcypher.com/btc/tx/60a20bd93aa49ab4b28d514ec10b06e1829ce6818ec06cd3aabd013ebcdc4bb1/ es un ejemplo el resulve la de destino
        addresses = [scriptPubKey.type];
    }
    if (!addresses.length || addresses.length > 1) {
        console.log(entry);
        throw new Error('REVISAR DIRECCIONES');
    }
    return addresses;
}

async function* getBlockTransactions (noOrHash, out) {
    const block = await blockchain.block(noOrHash);
    let params = [];
    let { tx, height, nextblockhash } = block;
    out.nextblockhash = nextblockhash;
    out.height = height;
    for (let hash of tx) {
        params.push([hash, true]);
    }
    const txouts = [];
    const vouts = {};
    for await (tx of blockchain.rawTransactions(...params)) {
        const { time, vin, vout, hash } = tx;
        for (let entry of vout) {
            let { value } = entry;
            let addresses = getAddresses(entry);
            for (let address of addresses) {
                yield {
                    address,
                    value,
                    time,
                    block: height,
                    hash
                }
            }
        }
        for (let entry of vin) {
            if (entry.hasOwnProperty(COINBASE)) {
                yield {
                    address: COINBASE,
                    value: - vout[0].value,
                    time,
                    block: height,
                    hash
                }
            } else if (entry.hasOwnProperty('txid')) {
                let { txid, vout } = entry;
                if (!vouts.hasOwnProperty(txid)) {
                    vouts[txid] = {};
                }
                txouts.push([txid, vout]);
                vouts[txid][vout] = {
                    txid: hash,
                    time
                };
            } else {
                throw new Error('Invalid from');
            }
        }
    }
    if (txouts.length) {
        for await (let entry of blockchain.txouts(...txouts)) {
            const { hash, vout, value } = entry;
            const { time, txid } = vouts[hash][vout];
            const addresses = getAddresses(entry);
            for (const address of addresses) {
                yield {
                    address,
                    value: - value,
                    time,
                    block: height,
                    hash: txid
                };
            }
        }
    }
}

exports.getBlockTransactions = getBlockTransactions;
