const { Blockchain } = require('btc-core');
const { Database } = require('db-core');
require('dotenv').config();

const blockchain = new Blockchain(process.env.RPC_USERNAME, process.env.RPC_PASSWORD);
const database = new Database(
    `mysql:dbname=${process.env.DB_NAME}`,
    process.env.DB_USERNAME,
    process.env.DB_PASSWORD
);

let now;
let lastNow;
let CACHE = {
    length: 0
};
let MAX_SCAN = 10000;
let finished = false;
const LOG_ON = 1;
const ROWS = [];

async function* getBlockAddresses (noOrHash, out) {
    const block = await blockchain.block(noOrHash);
    let { tx, height, nextblockhash, nTx } = block;
    out.nextblockhash = nextblockhash;
    out.height = height;
    out.nTx = nTx;
    let rows;
    while (rows = tx.splice(0, 250)) {
        if (!rows.length) {
            break;
        }
        let params = [];
        for (let hash of rows) {
            params.push([hash, true]);
        }
        const txs = await blockchain.batch('getrawtransaction', ...params);
        for (let txOut of txs) {
            const { vout } = txOut;
            for (let entry of vout) {
                let addresses = getAddresses(entry);
                for (let address of addresses) {
                    yield address;
                }
            }
        }
    }
}

function getAddresses(entry) {
    const { scriptPubKey } = entry;
    let addresses = scriptPubKey.addresses || [scriptPubKey.type];
    if (scriptPubKey.type == 'multisig' && addresses.length > 1) { // revisar https://live.blockcypher.com/btc/tx/60a20bd93aa49ab4b28d514ec10b06e1829ce6818ec06cd3aabd013ebcdc4bb1/ es un ejemplo el resulve la de destino
        addresses = [scriptPubKey.type];
    }
    if (!addresses.length || addresses.length > 1) {
        console.log(entry);
        throw new Error('REVISAR DIRECCIONES');
    }
    return addresses;
}

function logblock(no = 0, err = null) {
    console.log('TIME: ' + ((Date.now() - now) / 1000));
    if (no) {
        console.log('LAST BLOCK: ' + no);
    }
    if (err) {
        console.log(err);
    }
}

function round (value) {
    return Math.round(value * 100) / 100;
}


async function loop (numOrHash, counter, blockNum) {
    if (counter > MAX_SCAN) {
        logblock(blockNum - 1);
        finished = true;
        return;
    }
    let block = { nextblockhash: null, height: null, nTx: null };
    try {
        for await (let address of getBlockAddresses(numOrHash, block)) {
            ROWS.push(address);
        }
        // console.log(block.height + ' -> ' + block.nTx);
        if (counter % LOG_ON === 0) {
            let curNow = Date.now();
            let timeNow = ((curNow - lastNow) / 1000);
            let timeTotal = (curNow - now) / 1000;
            console.log('| ' + ((new Date).toLocaleString()) + ' | LAST BLOCK: ' +
                block.height + ` | ${LOG_ON} BLOCKS IN ` + round(timeNow) + ' SEC ->' +
                ' 1 BLOCK IN ' + round(timeNow / LOG_ON) + ' SEC |' +
                ' ' + counter + ' BLOCKS IN' +
                ' ' + round(timeTotal) + ' SEC ->' +
                ' 1 BLOCK IN ' + round(timeTotal / counter) + ' SEC |'
            );
        }
    } catch (e) {
        logblock(blockNum, e);
        process.exit();
    }
    if (block && block.nextblockhash) {
        lastNow = Date.now();
        setTimeout(loop.bind(null, block.nextblockhash,  ++ counter, block.height + 1), 0);
    } else {
        logblock(blockNum);
        process.exit();
    }
}

async function importAddress(hash) {
    let _id = CACHE[hash];
    if (_id) {
        if (CACHE.length >= 1000) {
            CACHE = {
                length: 0
            };
        }
        return _id;
    }
    let [ {id } ] = await database.query(
        `SELECT get_address_id_by_name('${hash}') as id`
    );
    CACHE.length ++;
    return CACHE[hash] = id;
}


async function insert () {
    if (finished && !ROWS.length) {
        logblock(0, ((new Date).toLocaleString()) + ' END');
        process.exit();
    }
    if (ROWS.length) {
        const entry = ROWS.shift();
        try {
            await importAddress(entry);
        } catch (e) {
            logblock(0, e);
            process.exit();
        }
    }
    setTimeout(insert, 0);
}

setTimeout(async () => {
    if (process.argv.length === 3) {
        let max = parseInt(process.argv[3]);
        if (max > 0) {
            MAX_SCAN = max;
        }
    }
    const start = 304855;
    console.log(((new Date).toLocaleString()) + ' START BLOCK ' + start);
    lastNow = now = Date.now();
    setTimeout(loop.bind(null, start, 1, start), 0);
    setTimeout(insert, 0);
}, 0);
